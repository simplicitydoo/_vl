<?php
/**
 * _VL Engine Room
 *
 * @package _vl
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {

    $content_width = 1152; /* pixels */

}

require get_theme_file_path( 'inc/vl-template-functions.php' );

$_vl = (object) [
    'main' => require get_theme_file_path( 'inc/class-vl.php' ),
    'api'  => require get_theme_file_path( 'inc/class-vl-api.php' )
];