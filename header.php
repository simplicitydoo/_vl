<?php
/**
 * The template for displaying the header.
 *
 * @package _vl
 */
?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=2.0">

    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>