<?php
/**
 * _VL Template Functions
 *
 * @package _vl
 */

if ( ! function_exists( '_vl_comment' ) ) {

    /**
     * Comment Template
     *
     * @param $comment
     * @param $args
     * @param $depth
     *
     * @since   1.0.0
     * @version 1.0.0
     */
    function _vl_comment( $comment, $args, $depth )
    {

        $comment_class = [
            'comments-area__comments-list__comment'
        ];
        if ( ! empty( $args['has_children'] ) ) {

            array_push( $comment_class, 'parent' );

        }

        ?>

        <li <?php comment_class( $comment_class ); ?> id="comment-<?php comment_ID(); ?>">
            <header class="media comments-area__comments-list__comment__header">
                <div class="media-left">
                    <figure class="image is-48x48">
                        <?php echo get_avatar( $comment, 48 ); ?>
                    </figure>
                    <div class="media-content">
                        <?php printf( wp_kses_post( '<cite class="fn">%s</cite>' ), get_comment_author_link() ); ?>
                        <?php echo '<time datetime="' . get_comment_date( 'c' ) . '">' . get_comment_date() . '</time>'; ?>
                    </div>
                </div>
            </header>
            <div class="comments-area__comments-list__comment__content content">
                <?php comment_text(); ?>
            </div>
            <footer class="comments-area__comments-list__comment__footer">
                <div class="comments-area__comments-list__comment__footer__button comments-area__comments-list__comment__footer__button--reply-link">
                    <?php
                    comment_reply_link(
                        array_merge(
                            $args, array(
                                'add_below' => 'comment',
                                'depth'     => $depth,
                                'max_depth' => $args['max_depth'],
                            )
                        )
                    );
                    ?>
                </div>
                <div class="comments-area__comments-list__comment__footer__button comments-area__comments-list__comment__footer__button--reply-link">
                    <?php edit_comment_link( esc_html__( 'Edit', '_vl' ), '', '' ); ?>
                </div>
            </footer>
        </li>

        <?php

    }

}