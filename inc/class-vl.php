<?php
/**
 * _VL Class
 *
 * @since   1.0.0
 * @package _vl
 */

if ( ! defined( 'ABSPATH' ) ) {

    exit;

}

if ( ! class_exists( '_VL' ) )
{

    /**
     * The Main _VL Class
     */
    class _VL {

        /**
         * Setup Class
         *
         * @since   1.0.0
         * @version 1.0.0
         */
        public function __construct()
        {

            add_action( 'after_setup_theme', [ $this, '_vl_after_setup_theme' ] );
            add_action( 'widgets_init', [ $this, '_vl_widgets_init' ] );
            add_action( 'wp_enqueue_scripts', [ $this, '_vl_wp_enqueue_scripts' ], 10 );

            // Change WP Core Things
            add_filter( 'comment_form_defaults', [ $this, '_vl_comment_form_defaults' ] );
            add_filter( 'comment_form_default_fields', [ $this, '_vl_comment_form_default_fields' ] );
            add_filter( 'previous_comments_link_attributes', [ $this, '_vl_previous_comments_link_attributes' ] );
            add_filter( 'next_comments_link_attributes', [ $this, '_vl_next_comments_link_attributes' ] );

        }

        /**
         * Sets up theme defaults and registers support for various WordPress features.
         *
         * @since   1.0.0
         * @version 1.0.0
         */
        public function _vl_after_setup_theme()
        {

            /**
             * Load Localisation files.
             */
            load_theme_textdomain( '_vl', get_template_directory() . '/languages' );

            /**
             * Add default posts and comments RSS feed links to head.
             */
            add_theme_support( 'automatic-feed-links' );

            /*
	     * Enable support for featured images.
	     */
            add_theme_support( 'post-thumbnails' );

            /**
             * Register menu locations.
             */
            register_nav_menus(
                [
                    'header' => esc_html__( 'Header Menu', '_vl' )
                ]
            );

            /*
	     * Switch default core markup for search form, comment form, comments, galleries, captions and widgets
	     * to output valid HTML5.
	     */
            add_theme_support(
                'html5', [
                    'search-form',
                    'comment-form',
                    'comment-list',
                    'gallery',
                    'caption',
                    'widgets',
                ]
            );

            /**
             * Declare support for title theme feature.
             */
            add_theme_support( 'title-tag' );

            /**
             * Declare support for selective refreshing of widgets.
             */
            add_theme_support( 'customize-selective-refresh-widgets' );

            /**
             * Add support for Block Styles.
             */
            add_theme_support( 'wp-block-styles' );

            /**
             * Add support for full and wide align images.
             */
            add_theme_support( 'align-wide' );

            /**
             * Add support for editor styles.
             */
            add_theme_support( 'editor-styles' );

            /**
             * Add support for responsive embedded content.
             */
            add_theme_support( 'responsive-embeds' );

            /**
             * Add support for WooCommerce.
             */
            add_theme_support( 'woocommerce' );
            add_theme_support( 'wc-product-gallery-zoom' );
            add_theme_support( 'wc-product-gallery-lightbox' );
            add_theme_support( 'wc-product-gallery-slider' );

        }

        /**
         * Register widget area.
         *
         * @since   1.0.0
         * @version 1.0.0
         */
        public function _vl_widgets_init()
        {

            $sidebar_args['posts_sidebar'] = [
                'name'        => esc_html__( 'Posts Sidebar', '_vl' ),
                'id'          => 'posts-sidebar',
                'description' => '',
            ];

            $sidebar_args['pages_sidebar'] = [
                'name'        => esc_html__( 'Pages Sidebar', '_vl' ),
                'id'          => 'pages-sidebar',
                'description' => '',
            ];

            foreach ( $sidebar_args as $sidebar => $args ) {

                $widget_tags = array(
                    'before_widget' => '<div id="%1$s" class="widget %2$s">',
                    'after_widget'  => '</div>',
                    'before_title'  => '<h2 class="widget__title">',
                    'after_title'   => '</h2>',
                );

                if ( is_array( $widget_tags ) ) {

                    register_sidebar( $args + $widget_tags );

                }

            }

        }

        /**
         * Enqueue scripts and styles.
         *
         * @since   1.0.0
         * @version 1.0.0
         */
        public function _vl_wp_enqueue_scripts()
        {

            /**
             * Styles
             */
            wp_enqueue_style( '_vl-style', get_theme_file_uri( 'style.css' ), '', '1.0.0' );

            /**
             * Fonts
             */
            wp_enqueue_style( '_vl-fontawesome-pro', get_theme_file_uri( 'assets/css/fontawesome-pro-all.min.css' ), '', '5.6.3' );

            /**
             * Scripts
             */
            wp_enqueue_script( '_vl-axios', get_theme_file_uri( 'assets/js/axios.min.js' ), [ 'jquery' ], '0.18.0', true );
            wp_enqueue_script( '_vl-script', get_theme_file_uri( 'assets/js/vl.js' ), [ 'jquery' ], '1.0.0', true );
            if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {

                wp_enqueue_script( 'comment-reply' );

            }

        }

        /**
         * Change WP Comment Form Defaults
         *
         * @param  $defaults
         * @return array
         *
         * @since   1.0.0
         * @version 1.0.0
         */
        public function _vl_comment_form_defaults( $defaults )
        {

            $defaults['comment_field'] = '<div class="field field--comment"><label for="comment" class="label">' . esc_html__( 'Comment', '_vl' ) . '</label><div class="control"><textarea id="comment" class="textarea" name="comment" cols="45" rows="8" maxlength="65525" required="required"></textarea></div></div>';
            $defaults['class_submit'] = 'button button--submit';
            $defaults['submit_button'] = '<button type="submit" id="%2$s" class="%3$s">%4$s</button>';
            $defaults['submit_field'] = '<div class="field field--submit">%1$s %2$s</div>';

            return $defaults;

        }

        /**
         * Change WP Comment Fields HTML
         *
         * @param  $fields
         * @return array
         *
         * @since   1.0.0
         * @version 1.0.0
         */
        public function _vl_comment_form_default_fields( $fields )
        {

            $commenter = wp_get_current_commenter();
            $req       = get_option( 'require_name_email' );
            $html_req  = ( $req ? " required='required'" : '' );

            $vl_fields = [
                'author' => '<div class="field field--author"><label for="author" class="label">' . esc_html__( 'Name', '_vl' ) . ( $req ? ' <span class="field__required">*</span>' : '' ) . '</label><div class="control"><input id="author" class="input" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30" maxlength="245" ' . $html_req . ' /></div></div>',
                'email'  => '<div class="field field--email"><label for="email" class="label">' . esc_html__( 'Email', '_vl' ) . ( $req ? ' <span class="field__required">*</span>' : '' ) . '</label><div class="control"><input id="email" class="input" name="email" type="email" value="' . esc_attr( $commenter['comment_author_email'] ) . '" size="30" maxlength="100" aria-describedby="email-notes" ' . $html_req . ' /></div></div>',
                'url'    => '<div class="field field--url"><label for="url" class="label">' . esc_html__( 'Website', '_vl' ) . '</label><div class="control"><input id="url" class="input" name="url" type="url" value="' . esc_attr( $commenter['comment_author_url'] ) . '" size="30" maxlength="200" /></div></div>'
            ];

            if ( isset( $fields['cookies'] ) && ! empty( $fields['cookies'] ) ) {

                $consent = empty( $commenter['comment_author_email'] ) ? '' : ' checked="checked"';

                $vl_fields['cookies'] = '<div class="field field--cookies"><input id="wp-comment-cookies-consent" class="is-checkradio" name="wp-comment-cookies-consent" type="checkbox" value="yes" ' . $consent . ' /><label for="wp-comment-cookies-consent">' . esc_html__( 'Save my name, email, and website in this browser for the next time I comment.', '_vl' ) . '</label></div>';

            }

            $fields = $vl_fields;

            return $fields;

        }

        /**
         * Add class to comments link.
         *
         * @param  $attributes
         * @return string
         *
         * @since   1.0.0
         * @version 1.0.0
         */
        public function _vl_previous_comments_link_attributes( $attributes )
        {

            $attributes = 'class="pagination-previous"';

            return $attributes;

        }

        /**
         * Add class to comments link.
         *
         * @param  $attributes
         * @return string
         *
         * @since   1.0.0
         * @version 1.0.0
         */
        public function _vl_next_comments_link_attributes( $attributes )
        {

            $attributes = 'class="pagination-previous"';

            return $attributes;

        }

    }

}

return new _VL();
