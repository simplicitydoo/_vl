<?php
/**
 * _VL Functions
 *
 * @package _vl
 */

if ( ! function_exists( '_vl_is_woocommerce_activated' ) ) {

    /**
     * Check if WooCommerce is activated.
     *
     * @return bool
     *
     * @since   1.0.0
     * @version 1.0.0
     */
    function _vl_is_woocommerce_activated()
    {

        return class_exists( 'WooCommerce' ) ? true : false;

    }

}

if ( ! function_exists( '_vl_is_product_archive' ) ) {

    /**
     * Check if is WooCommerce Product Archive.
     *
     * @return bool
     *
     * @since   1.0.0
     * @version 1.0.0
     */
    function _vl_is_product_archive()
    {

        if ( is_shop() || is_product_taxonomy() || is_product_category() || is_product_tag() ) {

            return true;

        } else {

            return false;

        }

    }

}

if ( ! function_exists( '_vl_is_blog' ) ) {

    /**
     * Check if is Blog page.
     *
     * @return bool
     *
     * @since   1.0.0
     * @version 1.0.0
     */
    function _vl_is_blog()
    {

        return ( is_archive() || is_author() || is_category() || is_home() || is_single() || is_tag()) && 'post' == get_post_type();

    }

}