# _VL

_VL is a WordPress Starter Theme


## Theme structure

```shell
themes/_vl                    # → Root of your _vl theme
├── assets                    # → Front-End assets
│   ├── css                   # → Theme stylsheets from dependencies
│   ├── images                # → Theme images
│   ├── js                    # → Theme JS
│   ├── scss                  # → Theme SCSS
│   ├── webfonts              # → FontAwesome 5 Pro fonts
├── inc/                      # → Theme PHP
│   ├── api                   # → REST API Controllers
│   ├── class-vl.php          # → Theme setup and filters/functions
│   ├── class-vl-api.php      # → Initialization of REST API Controllers
│   ├── vl-functions.php      # → Theme helper functions
│   ├── vl-template-functions # → Theme template functions
├── languages/                # → Theme languages
├── node_modules/             # → Node.js packages (never edit)
├── template-parts/           # → Theme template parts
├── templates/                # → Theme templates
├── .gitignore                # → Git ignored files/folders
├── .npmrc                    # → NPM config file
├── 404.php                   # → The template for displaying 404 pages (not found)
├── archive.php               # → The template for displaying archive pages
├── comments.php              # → The template for displaying comments
├── footer.php                # → The template for displaying the footer
├── functions.php             # → Theme engine room
├── header.php                # → The template for displaying the header
├── index.php                 # → The main template file
├── LICENSE                   # → Theme license
├── package.json              # → Node.js dependencies and scripts
├── page.php                  # → The template for displaying all pages
├── README.md                 # → File for displaying README (this)
├── screenshot.png            # → Theme screenshot
├── search.php                # → The template for displaying search results pages
├── single.php                # → The template for displaying all single posts
├── webpack.mix.js            # → Laravel Mix configuration
```


## Theme development

* Run `yarn` from the theme directory to install dependencies
* Run `yarn run dev` from the theme directory to run all Laravel Mix processes one time without minification
* Run `yarn run prod` from the theme directory to run all Laravel Mix processes one time with minification
* Run `yarn run watch` from the theme directory to watch for file changes using Laravel Mix


* **Use** `axios` to send HTTP requests
* **Don't** use WP AJAX, instead, use REST API