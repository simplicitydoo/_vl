// Require Laravel Mix
let mix = require('laravel-mix');

// Set Publich Path
mix.setPublicPath( '/' );

// Default Options
mix.options({
  processCssUrls: false
});

// Copy CSS
mix.copy( 'node_modules/@fortawesome/fontawesome-pro/css/all.min.css', 'assets/css/fontawesome-pro-all.min.css' );

// Copy JS
mix.copy( 'node_modules/axios/dist/axios.min.js', 'assets/js/axios.min.js' );

// Copy Fonts
mix.copy( 'node_modules/@fortawesome/fontawesome-pro/webfonts', 'assets/webfonts' );

// Compile SCSS
mix.sass( 'style.scss', 'style.css' );