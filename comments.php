<?php
/**
 * The template for displaying comments.
 *
 * @package _vl
 */
if ( post_password_required() ) {

    return;

}
?>

<section class="comments-area" id="comments">
    <?php if ( have_comments() ) : ?>
        <h2 class="comments-area__title"><?php printf( esc_html( _nx( '%1$s thought on &ldquo;%2$s&rdquo;', '%1$s thoughts on &ldquo;%2$s&rdquo;', get_comments_number(), 'comments title', '_vl' ) ), number_format_i18n( get_comments_number() ), '<span>' . get_the_title() . '</span>' ); ?></h2>
        <ol class="comments-area__comments-list">
            <?php
            wp_list_comments(
                [
                    'style'      => 'ol',
                    'short_ping' => true,
                    'callback'   => '_vl_comment'
                ]
            );
            ?>
        </ol>
        <?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : ?>
            <nav class="pagination is-centered comments-area__pagination" role="navigation" aria-label="pagination">
                <?php previous_comments_link( esc_html__( 'Older Comments', '_vl' ) ); ?>
                <?php next_comments_link( esc_html__( 'Newer Comments', '_vl' ) ); ?>
            </nav>
        <?php endif; ?>
    <?php endif; ?>

    <?php if ( ! comments_open() && 0 !== intval( get_comments_number() ) && post_type_supports( get_post_type(), 'comments' ) ) : ?>
        <div class="notification"><?php esc_html_e( 'Comments are closed.', '_vl' ); ?></div>
    <?php endif; ?>
    <?php comment_form(); ?>
</section>
